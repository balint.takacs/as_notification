import 'package:as_notification/src/subclasses/channels.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class DefaultSettings {
  final AndroidInitializationSettings androidInitializationSettings =
      const AndroidInitializationSettings('ic_launcher');

  final IOSInitializationSettings iosInitializationSettings =
      const IOSInitializationSettings(
    defaultPresentAlert: true,
    defaultPresentBadge: true,
    defaultPresentSound: true,
    requestAlertPermission: true,
    requestBadgePermission: true,
    requestSoundPermission: true,
  );
}
