import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class DefaultChannelSettings {
   AndroidNotificationChannel defaultChannel = const AndroidNotificationChannel(
    "id",
    "name",
    description: "description",
    importance: Importance.max,
    enableLights: true,
    enableVibration: true,
    playSound: true,
    showBadge: true, 
  );
}
