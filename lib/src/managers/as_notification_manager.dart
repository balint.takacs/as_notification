import 'package:as_notification/src/subclasses/parent_manager_class.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class AsNotificationManager extends ParentManagerClass {
  AsNotificationManager.init(
    Set<String> rawResources, {
    InitializationSettings? settings,
    AndroidNotificationChannel? androidNotificationChannel,
  }) : super(
          rawResources,
        ) {
    setLocalNotificationClass();
    localNotifications.initialize(
      settings ??
          InitializationSettings(
            iOS: iosInitializationSettings,
            android: androidInitializationSettings,
          ),
    );
    if (androidNotificationChannel != null) {
      defaultChannel = androidNotificationChannel;
    }
    createAndroidChannelsByRawResources();
  }
}
