import 'dart:io';

import 'package:as_notification/src/settings/channels.dart';
import 'package:as_notification/src/subclasses/base.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

mixin ChannelsSub on Base {
  Future<void> createAndroidChannelsByRawResources() async {
    if (Platform.isIOS) {
      debugPrint("Platform is iOS - channel create doesnt needed.");
      return;
    }
    for (int x = 0; x < rawResources.length; x++) {
      String indexedResource = rawResources.toList()[x];

      AndroidNotificationChannel notificationChannel =
          _createChannelByRawResource(
        id: indexedResource,
        name: indexedResource,
        description: indexedResource,
      );

      await localNotifications
          .resolvePlatformSpecificImplementation<
              AndroidFlutterLocalNotificationsPlugin>()
          ?.createNotificationChannel(
            notificationChannel,
          );
    }
  }

  AndroidNotificationChannel _createChannelByRawResource({
    required String id,
    required String name,
    required String description,
  }) { 
    return  AndroidNotificationChannel(
      id,
      name,
      description: description,
      importance: defaultChannel.importance,
      enableLights: defaultChannel.enableLights,
      enableVibration: defaultChannel.enableVibration,
      playSound: defaultChannel.playSound,
      showBadge: defaultChannel.showBadge,
      sound: defaultChannel.sound,
    );
  }
}
