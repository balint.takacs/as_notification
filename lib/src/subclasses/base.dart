import 'package:as_notification/src/settings/channels.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

abstract class Base extends DefaultChannelSettings {
  Base(rawResources);

  Set<String> rawResources = <String>{};

  late FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin;
  FlutterLocalNotificationsPlugin get localNotifications {
    return _flutterLocalNotificationsPlugin;
  }
  
  void setLocalNotificationClass(){
    _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
  }
}
