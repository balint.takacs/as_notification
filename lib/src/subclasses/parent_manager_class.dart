import 'package:as_notification/src/settings/settings.dart';
import 'package:as_notification/src/subclasses/base.dart';
import 'package:as_notification/src/subclasses/channels.dart';
import 'package:as_notification/src/subclasses/handle.dart';
import 'package:as_notification/src/subclasses/permission.dart';

class ParentManagerClass extends Base
    with ChannelsSub, HandleSub, PermissionSub, DefaultSettings {
  ParentManagerClass(rawResources) : super(rawResources);
}
