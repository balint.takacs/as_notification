import 'package:as_notification/src/models/notification_model.dart';
import 'package:as_notification/src/settings/settings.dart';
import 'package:as_notification/src/subclasses/base.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

mixin HandleSub on Base {
  Set<NotificationModel> receivedNotifications = <NotificationModel>{};

  Future<void> showNotification(NotificationModel notification) async {
    String channelId = notification.channelId;
    receivedNotifications.add(notification);

    await localNotifications.show(
      notification.hashCode,
      notification.title,
      notification.body,
      NotificationDetails(
        android: AndroidNotificationDetails(
          channelId,
          channelId,
        ),
      ),
    );
  }

  Future<void> removeNotification({
    required int id,
  }) async {
    receivedNotifications.removeWhere((element) => element.id == id);
    await localNotifications.cancel(id);
  }
}
