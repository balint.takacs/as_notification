import 'dart:io';

import 'package:as_notification/src/subclasses/base.dart';
import 'package:as_notification/src/subclasses/handle.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

mixin PermissionSub on Base {
  Future<bool?> requestPermission({
    bool sound = false,
    bool alert = false,
    bool badge = false,
  }) async {
    if (!Platform.isIOS) {
      throw UnsupportedError("Unsupported platform.");
    }
    return await localNotifications
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(sound: sound, alert: alert, badge: badge);
  }
}
