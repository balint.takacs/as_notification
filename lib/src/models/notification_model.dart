class NotificationModel {
  final int id;

  final String title;

  final String body;

  final String channelId;

  final Map<String, Object> data;

  NotificationModel(
    this.id, {
    required this.title,
    required this.body,
    required this.channelId,
    required this.data,
  });
}
