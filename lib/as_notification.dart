library as_notification;

import 'package:as_notification/src/managers/as_notification_manager.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class AsNotification {
  static AsNotificationManager? _instance;
  static AsNotificationManager get instance {
    return _instance!;
  }

  static bool get isInited => _instance != null;

  static void init(
    Set<String> rawResources, {
    InitializationSettings? settings,
  }) {
    if (isInited) {
      debugPrint("Already inited.");
      return;
    }

    _instance = AsNotificationManager.init(
      rawResources,
      settings: settings,
    );
  }
}
